<?php
namespace Minc\MincOMS\Controller\Adminhtml\SampleTwo; 

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Config\ScopeConfigInterface; // Needed to retrieve config values

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
     protected $resultPageFactory;

    /**
     * @var scopeConfig
     * Needed to retrieve config values
     */
    protected $scopeConfig;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ScopeConfigInterface $scopeConfig // Needed to retrieve config values
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig; // Needed to retrieve config values
    }

    /**
    * Index Action*
    * @return void
    */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Minc_MincOMS::sampleTwo');
        $resultPage->getConfig()->getTitle()->prepend(__('Minc OMS')); 
        $resultPage->getLayout()->getBlock('Two')->setSampleText('This text is passed'); 
        $cfg_text = $this->scopeConfig->getValue('mincoms/txt/textsample');
        $resultPage->getLayout()->getBlock('Two')->setCfgSample($cfg_text);
        return $resultPage;
    }
}
