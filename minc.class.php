<?php

class minc {

    //array to json//
    public function arrayToJson($data) {       
        return json_encode($data);
    }
    
    //json to array//
    public function jsonToArray($data) {
        return json_decode($data);    
    }
    
    public function xmlToArray($xml) {
        
        $xml = simplexml_load_string(file_get_contents($xml));
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        
        return $array;
    }
    
    //address parser//
    function addressParser($address) {
       
        $address = strip_tags($address);
        $address = str_replace("\n"," ", $address);
        preg_match('/(?P<address>\D+)(?P<number>\d+)(?P<numberAdd>.*)/', $address, $addressClear);

	//Other format (UK,FR ect)//
	if(count($addressClear) == 0)
	preg_match('/(?P<number>\d+)(?P<address>\D+)(?P<numberAdd>.*)/', $address, $addressClear);

	//parser empty -> full address//
	if(count($addressClear) == 0 && !empty($address))
	$addressClear['address'] = $address;

        return $addressClear;
    }
    
    //parse order//
    public function parseOrder($global, $additionalDeliveryInfo = "") {

                //address info//
                $address = $global->getShippingAddress();
        
                //order id AND globale info//
                $orders['id'] = $global['increment_id'];
                $orders['email'] = $global['customer_email'];
                $orders['telephone'] = $address['telephone'];
                
		//add custom note//
		$orders['note'] = "shipping:".$global['shipping_description'];

                //recipientName//
                $orders['recipientName'] = array(
                    companyName => $address['company'],
                    firstName => $address['firstname'],
                    surnamePrefix => $address['prefix'],
                    lastName => $address['lastname']
                  );
                //filter empty values//
                $orders['recipientName'] = array_filter($orders['recipientName']);

                //deliveryAddress//
                $orders['deliveryAddress'] = array(
                    street => trim($this->addressParser($global->getShippingAddress()['street'])['address']),  
                    streetNo => $this->addressParser($global->getShippingAddress()['street'])['number'],   
                    streetNoAddition => trim($this->addressParser($global->getShippingAddress()['street'])['numberAdd']),  
                    postalcode => $global->getShippingAddress()['postcode'],
                    city => $global->getShippingAddress()['city'],  
                    country => $global->getShippingAddress()['country_id']
                );
                //filter empty values//
                $orders['deliveryAddress'] = array_filter($orders['deliveryAddress']);

                //delivery service//
                $shipping = explode("_", $global->getShippingMethod());
                $orders['deliveryService'] = array(
                    company => $shipping[0],
                    service => $shipping[1]
                );
                $orders['deliveryService'] = array_filter($orders['deliveryService']);

                //items// 
                $orders['items'] = array();
                foreach($global['items'] as $kItem) {
                    $itemsObject = array(
                        product => $kItem->getSku(),
                        quantity => round($kItem->getQtyOrdered())                
                    );
                    array_push($orders['items'], $itemsObject);
                }
                
                //order value//
                $orders['value'] = number_format(money_format('%.2n', $global['base_subtotal']), 0, '', '');

		//add gift message//
		if(strlen($global->getGiftMessages()->message) >= 1) {
		$orders['dataEntries'] = array();
			array_push($global->getGiftMessages(), array("name" => "getGiftMessage_sender", "entryValue" => $global->getGiftMessages()['getGiftMessage']->sender));
			array_push($global->getGiftMessages(), array("name" => "getGiftMessage_recipient", "entryValue" => $global->getGiftMessages()['getGiftMessage']->recipient));
			array_push($global->getGiftMessages(), array("name" => "getGiftMessage_message", "entryValue" => $global->getGiftMessages()['getGiftMessage']->message));
		}

                //add additional delivery Info//
                if(count($additionalDeliveryInfo) >= 1)
                foreach($additionalDeliveryInfo as $k => $v) {
                    $orders[$k] = $v;
                }
                
                return $orders;
    }
}


?>