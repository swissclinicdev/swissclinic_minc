<?php

//default bootstrap loaded//
require_once BP.'/app/bootstrap.php';

class magento {

        //include classes / objects//
        public function __construct() {

            $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
            $app = $bootstrap->createApplication('\Magento\Framework\App\Http');
            
            $this->objectManager =  \Magento\Framework\App\ObjectManager::getInstance();

            $state = $this->objectManager->get('Magento\Framework\App\State');
            $state->setAreaCode('global');

            //Minc class//
            require_once("minc.class.php");
            $this->minc = new minc;
        } 

    private $configPath = BP."/app/code/Minc/MincOMS/etc/minc_config.xml";

    //version//
    public function version(){
        
        return "v4"; 
    }
    
    //check key and state / auth//
    private function auth($request) {
        
        //get config//
        $config = $this->getConfig();
	$config = $config['modules']['MincMijdrecht_Minc'];
        
        //check key and state//
        if($request['key'] == $config['key'] && $config['enabled'] == 1) {
            return true;
        } else {
            header('HTTP/1.0 401 Forbidden');
            return false;
        }
    }
    
    //get magento statussen//
    public function getStatussen() {

            return $this->objectManager->get('Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory')->create()->toOptionArray();
    }

    //get stock list//
    public function stockFields() {

	$stockFields = array(
	"physicalTotal",
	"physicalAvailable",
	"physicalUnInitiated",
	"physicalExpired",
	"physicalDefect",
	"orderReserved",
	"applicationReserved",
	"backorderReserved",
	"availableForOrders",
	"backorderRequired",
	"notified",
	"currentlyAvailable",
	"futureAvailable",
	"backorderUnreserved");

	return $stockFields;
    }
    
    //get config data//
    public function getConfig() {
        return $this->minc->xmlToArray($this->configPath); 
    }
    
    //change config XML//
    private function changeConfigXML ($tag,$value){

        //load XML//
        $xml = file_get_contents($this->configPath);

        //replace value//
        $xml = preg_replace("/<".$tag.">.*?<\/".$tag.">/", "<".$tag.">".$value."</".$tag.">", $xml);
        
        //save XML file//
        file_put_contents($this->configPath,$xml);
    }
    
    //save settings//
    private function saveConfig($request) {

         if(strlen($request['key']) > 10 && strlen($request['form_key']) > 1) {

            //change key//
            $this->changeConfigXML("key",$request['key']);
            $this->changeConfigXML("enabled",$request['enabled'] ?: 0);
	    $this->changeConfigXML("enabledPostNL",$request['enabledPostNL'] ?: 0);
            
            $statusNew = "";
            foreach($request['statusNew'] as $k) {
                
                $statusNew .= $k.",";  
            }
            
            $this->changeConfigXML("statusNew",$statusNew);
            $this->changeConfigXML("statusProcessing",$request['statusProcessing']);         
            $this->changeConfigXML("statusBackorder",$request['statusBackorder']);  
            $this->changeConfigXML("statusCancel",$request['statusCancel']);
            $this->changeConfigXML("statusDone",$request['statusDone']); 
            $this->changeConfigXML("stockField",$request['stockField']); 
	    $this->changeConfigXML("statusOnhold",$request['statusOnhold']);
         }

         //errors//
         else {
            $error[] = "ERROR: save config";
            return $error;
         }
         
         return true;
    }
    
    //get accept order statuses//
    private function getStatuses(){
        
        //get Statuses config//
	$getConfig = $this->getConfig();

        return explode(",",$getConfig['modules']['MincMijdrecht_Minc']['statusNew']);
    }
    
    //GET export data to OMS//
    private function orders($request) {

           return $this->getOrders($this->getStatuses(), $request['shopid']); 
    }

    //get orders//
    private function getOrders($statuses, $storeid = 0) {

            $returnOrders = array();

            //get orders//
            $Mage = $this->objectManager->get('Magento\Sales\Model\Order')->getCollection();
            
            //add filters//
            if($storeid > 0)
                $orders = $Mage->addFieldToFilter("status", $statuses)->addFieldToFilter("store_id", intval($storeid))->load();
            else
                $orders = $Mage->addFieldToFilter("status", $statuses)->load();
            
            //each orders build data//
            foreach($orders as $k) {

                //order items to array//
                $k['items'] = $this->getOrderItems($k['increment_id']);

                //push array to return orders array//
                array_push($returnOrders, $this->minc->parseOrder($k, $additionalDeliveryInfo));     
            }


    //update last sync//
    $this->updateSync();
            
    return $returnOrders;    
    }

    //update status//
    private function updateStatus($data) {
                           
        $config = $this->getConfig();
	$config = $config['modules']['MincMijdrecht_Minc'];
        
        //accept status//
        $status = array(
            "GEANNULEERD" => "statusCancel",
            "INBEHANDELING" => "statusProcessing",
            "BACKORDER" => "statusBackorder",
            "AFGEHANDELD" => "statusDone",
	    "ONHOLD" => "statusOnhold"
        );

        //failures array//
        $messages['failures'] = array();
       
        foreach($data as $k) {


                    //create or update a SHIPMENT//
		    $updateShipments = $this->updateShipments(array("shipments" => $k->shipments, "orderId" => $k->id));

                    //change status//
		    if(count($status[$k->status]) > 0) {  
			try {                            
                            $order = $this->objectManager->get('Magento\Sales\Model\Order')->loadByIncrementID($k->id);
                            $order->setStatus($config[$status[$k->status]], "", true);
                            $order->save();  
                        }
			catch (\Error $e) {
                            array_push($messages['failures'], array(id => $k->id, message => "updateStatus -> ".$e));   
			}
		    }
                    //wrong status//
                    else
                        array_push($messages['failures'], array(id => $k->id, message => "wrong status ".$k->status));

                    //foreach errors//
		    foreach($updateShipments as $updateShipment)
                    array_push($messages['failures'], array(id => $updateShipment['id'], message => $updateShipment['message']));
        }

        $messages['failures'] = array_filter($messages['failures']);
        
        //failures//
        if(count($messages['failures']) >= 1) {
            $messages['success'] = false;
            return $messages;
        }
        else {
            $messages['success'] = true;
            return $messages;
        }
    }
    
    private function getOrderItems($incrementId) {
        
        $orderItemData = $this->objectManager->get('Magento\Sales\Model\Order')->loadByIncrementId($incrementId);
        $orderItems = $orderItemData->getAllVisibleItems();
        
        return $orderItems;
    }
    
    private function getShipments($orderId) {
	
	//get new data for shipments//
        $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
        $app = $bootstrap->createApplication('\Magento\Framework\App\Http');
        $this->objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $state = $this->objectManager->get('Magento\Framework\App\State');
        $state->setAreaCode('global');
	//////////////////////////////
        
        //shipments to array//
        $shipmentChecks = $this->objectManager->get('Magento\Sales\Model\Order')
                ->loadByIncrementId($orderId)
                ->getShipmentsCollection();
        
        foreach($shipmentChecks as $shipmentCheck)
             $currentShipments[$shipmentCheck->GetShippingLabel()] = $shipmentCheck->GetId();
        
        return $currentShipments;
    }
    
    private function updateShipments($data) {
        
        //failures array//
        $messages['failures'] = array();

        //get shipments//
        $currentShipments = $this->getShipments($data['orderId']);

                    //create or update a SHIPMENT//
                    foreach($data['shipments'] as $shipment) {

                        //#########create shipment#########//
                        if($shipment->status == 'SENT') {
                                try {                            

                                    //check shipment not exists// 
                                    if(intval($currentShipments[$shipment->id]) == 0) {

                                        //shipment items//
                                        $items = array();
                                        foreach($shipment->shipmentLines as $shipmentLines) {
                                            $items[$shipmentLines->productId]['quantity'] = $shipmentLines->quantity;
                                        }

                                        ##################################SHIPMENT#######################################
                                        // Load the order
                                        $order = $this->objectManager->create('Magento\Sales\Model\Order')
                                            ->loadByAttribute('increment_id', $data['orderId']);


                                            // Initialize the order shipment object
                                            $convertOrder = $this->objectManager->create('Magento\Sales\Model\Convert\Order');
                                            $shipmentCreate = $convertOrder->toShipment($order);

                                            //add shipment label//
                                            $shipmentCreate->setData('shipping_label', $shipment->id);

                                            // Loop through order items
                                            foreach ($order->getAllItems() AS $orderItem) {

                                                //check SKU in shipment//
                                                if($items[$orderItem->getSku()]['quantity'] >= 1) {

                                                    $qtyShipped = $orderItem->getQtyToShip();
                                                    $qtyShipped = $items[$orderItem->getSku()]['quantity'];

                                                    // Create shipment item with qty
                                                    $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);

                                                    // Add shipment item to shipment
                                                    $shipmentCreate->addItem($shipmentItem);   
                                                }
                                            }

                                            // Register shipment
                                            $shipmentCreate->register();

                                            $shipmentCreate->getOrder()->setIsInProcess(true);

                                            // Save created shipment and order
                                            $shipmentCreate->save();
                                            $shipmentCreate->getOrder()->save();

                                            // Send email
                                            $this->objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
                                                ->notify($shipmentCreate);

                                            $shipmentCreate->save();

                                    }
                                        ##################################END#SHIPMENT###################################
                                        } catch (\Error $e) {
                                            array_push($messages['failures'], array(id => $data['orderId'], message => "createShipment -> ".$e->getMessage()));
                                        }

					//add track and trace//
					if(count($shipment->tracktrace) >= 1) { 
						$addTrackAndTrace = $this->addTrackAndTrace(array("tracktrace" => $shipment->tracktrace, "shipmentId" => $shipment->id, "orderId" => $data['orderId']));
			       
						foreach($addTrackAndTrace as $kk)
							array_push($messages['failures'], array(id => $kk['id'], message => $kk['message']));
					}

                        }
                    }      
                    
        return array_filter($messages['failures']);
    }

    //add track and trace to shipment//
    private function addTrackAndTrace($data) {

        //failures array//
        $messages['failures'] = array();

        //get shipment information//
        $order = $this->objectManager->get('Magento\Sales\Model\Order')
                ->loadByIncrementId($data['orderId']);

        //get all track & trace by orderId//
        $trackNumber = array();
        foreach ($order->getTracksCollection() as $track){
            $trackNumbers[] = $track->getNumber();
        }
        
        //get shipments//
        $shipments = $this->getShipments($data['orderId']);

        $shipmentId = $shipments[$data['shipmentId']];

        if($shipmentId >= 1) {
            foreach($data['tracktrace'] as $trackAndTrace) {

                try {
                    //add track & trace if not exist//
                    if(!in_array($trackAndTrace->code, $trackNumbers)) {

                            $shipmentData = $this->objectManager->create('Magento\Sales\Model\Order\Shipment')->load($shipmentId);
                            $track = $this->objectManager->create('Magento\Sales\Model\Order\Shipment\Track')
                                    ->setShipment($shipmentData)
                                    ->setTitle($trackAndTrace->service." ".$trackAndTrace->shipper)
                                    ->setNumber($trackAndTrace->code)
                                    ->setCarrierCode($trackAndTrace->shipper)
                                    ->setOrderId($shipmentData->getData('order_id'))
                                    ->save();

                    }
                }
                catch (\Error $e) {
                    array_push($messages['failures'], array(id => $data['orderId'], message => "addTrackAndTrace -> ".$e->getMessage()));   
                }                  
            } 
        }
        
        return array_filter($messages['failures']);
    }

    //update stock / quantity product//
    private function updateStock($data) {

        $config = $this->getConfig();
	$config = $config['modules']['MincMijdrecht_Minc'];
        
        //failures array//
        $messages['failures'] = array();
        
        foreach($data as $k) {

            //try quantity update//
            try { 
            
                //get quantity//
                $qty = intval($k->{$config['stockField']});

                //select product//
                $product = $this->objectManager->get('Magento\Catalog\Model\Product')->loadByAttribute('sku',$k->productId);

                //update quantity//
                $stockItem= $this->objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($product->getId()); // load stock of that product
                $stockItem->setData('qty',$qty); //set updated quantity 
                    
                //set in stock more then 1//
                if($qty >= 1) {
                    $stockItem->setData('is_in_stock',1); //set updated data as your requirement
                    $stockItem->setData('manage_stock',1);
                    $stockItem->setData('use_config_notify_stock_qty',1);
                }

                $stockItem->save(); //save stock of item
                $product->save(); //  also save product

            }
            catch (\Error $e) {
                //array_push($messages['failures'], array(sku => $k->productId, message => "stockupdates -> ".$e->getMessage()));   
            }  
        }
        
        //failures//
        if(count($messages['failures']) >= 1) {
            $messages['success'] = false;
            return $messages;
        }
        else {
            $messages['success'] = true;
            return $messages;
        }
    }

    //update last sync//
    private function updateSync() {
	$this->changeConfigXML("lastRequest",date('d-m-Y G:i:s', time()));
    }

    //handler//
    public function handler($request) {
               
        //print_r($this->objectManager->get('Magento\Backend\Model\Auth\Session')->_adminSession());
        
        //save config//
        if(isset($request['saveMinc']))
            return $this->saveConfig($request);
        
        //GET new orders//      
        elseif($request['type'] == 'orders' && $this->auth($request))
            return $this->minc->arrayToJson($this->orders($request));
            
        //POST order updates//
        elseif($request['type'] == 'orderupdates' && $this->auth($request))   
            return $this->minc->arrayToJson($this->updateStatus($this->minc->jsonToArray($request['json'])));
            
        //POST stock updates//
        elseif($request['type'] == 'stockupdates' && $this->auth($request))
            return $this->minc->arrayToJson($this->updateStock($this->minc->jsonToArray($request['json'])));
        
        //POST stock updates//
        elseif($request['type'] == 'version')
            return $this->version();
        
    }
}
