<?php
namespace Minc\MincOMS\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Minc\MincOMS\Helper\ConfigHelper;


class CustomBlock extends Template
{
    /**
     * @var \Minc\MincOMS\Helper\ConfigHelper
     */
    protected $_config;


    /**
    * @param Context $context
    * @param array $data
    */
    public function __construct(
        Template\Context $context,
        ConfigHelper $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_config = $config;
    }

    public function greet()
    {
        return $this->_adminSampleModel->getGreetings();
    }

    public function getSampleText()
    {
        return $this->_adminSampleModel->getSampleText();
    }

}
