<?php

namespace Minc\MincOMS\Api\Data;

interface MincOMSInterface
{
    public function getGreetings();

    public function getSampleText();

}
