<?php

namespace Minc\MincOMS\Model;

use Minc\MincOMS\Api\Data\MincOMSInterface;
use Minc\MincOMS\Helper\ConfigHelper;

class MincOMS extends \Magento\Framework\Model\AbstractModel implements MincOMSInterface
{
    /** @var  ConfigHelper */
    protected $_config;

    public function __construct(ConfigHelper $config)
    {
        $this->_config = $config;

    }
    public function getGreetings()
    {
        return 'Greetings!';
    }

    public function getSampleText()
    {
        return $this->_config->getConfig('');
    }
}
